package com.example.quach.food.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.quach.food.R;
import com.example.quach.food.fragments.ManagerTabFragment;
import com.example.quach.food.fragments.categories.ProfileCategories;
import com.example.quach.food.fragments.categories.RecipesCategories;
import com.example.quach.food.fragments.categories.WineCategories;

/**
 * Created by quach on 13/07/17.
 */

public class CategoriesActivity extends AppCompatActivity {
    private TabLayout tlTabCategories;
    private ViewPager vpTabCategories;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        setTitle("Categories");
        initView();
    }

    private void initView() {
        tlTabCategories = (TabLayout) findViewById(R.id.tlTabCategories);
        vpTabCategories = (ViewPager) findViewById(R.id.vpTabCategories);
        settupViewPager(vpTabCategories);
        tlTabCategories.setupWithViewPager(vpTabCategories);
    }

    public void settupViewPager(ViewPager viewPager){
        ManagerTabFragment managerTabFragment = new ManagerTabFragment(getSupportFragmentManager());
        managerTabFragment.addFragment(new RecipesCategories(), "Recipes");
        managerTabFragment.addFragment(new WineCategories(), "Wine");
        managerTabFragment.addFragment(new ProfileCategories(), "Profiles");
        viewPager.setAdapter(managerTabFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionSearch) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
