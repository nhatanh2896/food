package com.example.quach.food.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.quach.food.R;

/**
 * Created by quach on 13/07/17.
 */

public class LoginActivity extends Activity implements View.OnClickListener {
    private ImageView imvLoginTwiter;
    private ImageView imvLoginFace;
    private Button btSingIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        imvLoginTwiter = (ImageView) findViewById(R.id.imvLoginTwiter);
        imvLoginFace = (ImageView) findViewById(R.id.imvLoginFace);
        btSingIn = (Button) findViewById(R.id.btSingIn);

        imvLoginFace.setOnClickListener(this);
        imvLoginTwiter.setOnClickListener(this);
        btSingIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
