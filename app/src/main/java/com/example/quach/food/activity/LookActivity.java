package com.example.quach.food.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.example.quach.food.R;
import com.example.quach.food.adapter.AdapterRecipesLook;

import java.util.ArrayList;

/**
 * Created by quach on 13/07/17.
 */

public class LookActivity extends AppCompatActivity {
    private RecyclerView rcvLook;
    private ArrayList<String> arrData;
    private AdapterRecipesLook adapterRecipesLook;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipeslook);
        setTitle("Recipes");
        arrData = new ArrayList<>();
        initData();
        initView();
    }

    private void initData() {
        arrData.add("@drawable/recipe1");
        arrData.add("@drawable/recipe2");
        arrData.add("@drawable/recipe1");
        arrData.add("@drawable/recipe1");
    }

    private void initView() {
        rcvLook = (RecyclerView) findViewById(R.id.rcvRecipesLook);
        adapterRecipesLook = new AdapterRecipesLook(arrData, this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rcvLook.setAdapter(adapterRecipesLook);
        rcvLook.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menureciption, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
