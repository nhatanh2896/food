package com.example.quach.food.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.quach.food.R;
import com.example.quach.food.fragments.ManagerTabFragment;
import com.example.quach.food.fragments.recipes.ProfilesFragment;
import com.example.quach.food.fragments.recipes.RecipesFragment;
import com.example.quach.food.fragments.recipes.WineFragment;

/**
 * Created by quach on 12/07/17.
 */

public class RecipesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private TabLayout tlRecipes;
    private ViewPager vpRecipes;
    private ManagerTabFragment managerTabFragment;
    private RecipesFragment recipesFragment;
    private WineFragment wineFragment;
    private ProfilesFragment profilesFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        managerTabFragment = new ManagerTabFragment(getSupportFragmentManager());
        recipesFragment = new RecipesFragment();
        wineFragment = new WineFragment();
        setTitle("Recipes");
        profilesFragment = new ProfilesFragment();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        initView();
    }

    private void initView() {
        tlRecipes = (TabLayout) findViewById(R.id.tlTabRecipes);
        vpRecipes = (ViewPager) findViewById(R.id.vpTabRecipes);
        setupViewPager(vpRecipes);
        tlRecipes.setupWithViewPager(vpRecipes);
    }

    public void setupViewPager(ViewPager viewPager){

        managerTabFragment.addFragment(recipesFragment, "Recipes");
        managerTabFragment.addFragment(wineFragment, "Wine");
        managerTabFragment.addFragment(profilesFragment, "Profile");
        viewPager.setAdapter(managerTabFragment);
    }

    public ManagerTabFragment getManagerTabFragment() {
        return managerTabFragment;
    }

    public void setManagerTabFragment(ManagerTabFragment managerTabFragment) {
        this.managerTabFragment = managerTabFragment;
    }

    public RecipesFragment getRecipesFragment() {
        return recipesFragment;
    }

    public void setRecipesFragment(RecipesFragment recipesFragment) {
        this.recipesFragment = recipesFragment;
    }

    public WineFragment getWineFragment() {
        return wineFragment;
    }

    public void setWineFragment(WineFragment wineFragment) {
        this.wineFragment = wineFragment;
    }

    public ProfilesFragment getProfilesFragment() {
        return profilesFragment;
    }

    public void setProfilesFragment(ProfilesFragment profilesFragment) {
        this.profilesFragment = profilesFragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionSearch) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
