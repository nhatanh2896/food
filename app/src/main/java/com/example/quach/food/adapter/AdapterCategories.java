package com.example.quach.food.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.quach.food.R;
import com.example.quach.food.activity.RecipesActivity;
import com.example.quach.food.models.Categories;

import java.util.ArrayList;

/**
 * Created by quach on 13/07/17.
 */

public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.ViewHolder> {
    private ArrayList<Categories> arrData;
    private LayoutInflater inflater;
    private Context context;

    public AdapterCategories(ArrayList<Categories> arrData, Context context) {
        this.arrData = arrData;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activityitemcategories, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Categories categories = arrData.get(position);
        holder.txtTitleCategories.setText(categories.getTitle());
        holder.txtNumber.setText(categories.getNumber()+"");
        int img = context.getResources().getIdentifier(categories.getLinkImg(), "drawable", context.getPackageName());
        Drawable drawable = context.getResources().getDrawable(img);
        holder.imvCategories.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imvCategories;
        TextView txtTitleCategories;
        TextView txtNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            imvCategories = (ImageView) itemView.findViewById(R.id.imvCategories);
            txtTitleCategories = (TextView) itemView.findViewById(R.id.txtTitleCategories);
            txtNumber = (TextView) itemView.findViewById(R.id.txtNumber);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, RecipesActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }
}
