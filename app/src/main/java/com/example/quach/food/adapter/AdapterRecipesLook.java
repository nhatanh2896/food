package com.example.quach.food.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.quach.food.R;

import java.util.ArrayList;

/**
 * Created by quach on 13/07/17.
 */

public class AdapterRecipesLook extends RecyclerView.Adapter<AdapterRecipesLook.ViewHolder> {
    private ArrayList<String> arrData;
    private LayoutInflater inflater;
    private Context context;

    public AdapterRecipesLook(ArrayList<String> arrData, Context context) {
        this.arrData = arrData;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activity_itemrecipeslook, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int img = context.getResources().getIdentifier(arrData.get(position), "drawable", context.getPackageName());
        Drawable drawable = context.getResources().getDrawable(img);
        holder.imvLook.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvLook;

        public ViewHolder(View itemView) {
            super(itemView);
            imvLook = (ImageView) itemView.findViewById(R.id.imvLook);
        }
    }
}
