package com.example.quach.food.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by quach on 12/07/17.
 */

public class ManagerTabFragment extends FragmentPagerAdapter {
    private ArrayList<Fragment> arrFragment =  arrFragment = new ArrayList<>();;
    private ArrayList<String> arrTitle = arrTitle = new ArrayList<>();;

    public ManagerTabFragment(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return arrFragment.get(position);
    }

    @Override
    public int getCount() {
        return arrFragment.size();
    }

    public void addFragment(Fragment fragment, String title){
        arrFragment.add(fragment);
        arrTitle.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return arrTitle.get(position);
    }
}
