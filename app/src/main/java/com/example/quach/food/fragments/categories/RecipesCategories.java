package com.example.quach.food.fragments.categories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quach.food.R;
import com.example.quach.food.adapter.AdapterCategories;
import com.example.quach.food.models.Categories;

import java.util.ArrayList;

/**
 * Created by quach on 13/07/17.
 */

public class RecipesCategories extends Fragment {
   private RecyclerView rcvCategories;
    private ArrayList<Categories> arrData;
    private AdapterCategories adapterCategories;
    private LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentcategoriesrecipes, container, false);
        rcvCategories = (RecyclerView) view.findViewById(R.id.rcvCategoriesrecipes);
        arrData = new ArrayList<>();
        initData();
        adapterCategories = new AdapterCategories(arrData, getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcvCategories.setAdapter(adapterCategories);
        rcvCategories.setLayoutManager(layoutManager);

        rcvCategories.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rcvCategories.setHasFixedSize(true);

        return view;
    }

    private void initData() {
        Categories c1 = new Categories("@drawable/cat1","Appetizer",78);
        Categories c2 = new Categories("@drawable/cat2","Bevarages",24);
        Categories c3 = new Categories("@drawable/cat3","Breakfasts",34);
        Categories c4 = new Categories("@drawable/cat4","Desserts",17);
        Categories c5 = new Categories("@drawable/cat5","Main Dishes",56);
        Categories c6 = new Categories("@drawable/cat6","Side Dishes",20);
        arrData.add(c1);
        arrData.add(c2);
        arrData.add(c3);
        arrData.add(c4);
        arrData.add(c5);
        arrData.add(c6);
    }
}
