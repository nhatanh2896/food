package com.example.quach.food.fragments.recipes;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quach.food.R;
import com.example.quach.food.adapter.AdapterRecipes;

import java.util.ArrayList;

/**
 * Created by quach on 12/07/17.
 */

public class RecipesFragment extends Fragment {
    private RecyclerView rcvRecipes;
    private ArrayList<com.example.quach.food.models.Recipes> arrData;
    private AdapterRecipes adapterRecipes;
    private LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentrecipesrecipes, container, false);
        rcvRecipes = (RecyclerView) view.findViewById(R.id.rcvRecipesRecipes);
        arrData = new ArrayList<>();
        initData();
        adapterRecipes = new AdapterRecipes(arrData, getActivity());
        rcvRecipes.setAdapter(adapterRecipes);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcvRecipes.setLayoutManager(layoutManager);
        rcvRecipes.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rcvRecipes.setHasFixedSize(true);
        return view;
    }

    private void initData() {
        com.example.quach.food.models.Recipes r1 = new com.example.quach.food.models.Recipes("Mandarian Sorbet", "by John Doe", "@drawable/img1","");
        com.example.quach.food.models.Recipes r2 = new com.example.quach.food.models.Recipes("Blackberry Sorbet", "by Stansil Kirilov", "@drawable/img2","");
        com.example.quach.food.models.Recipes r3 = new com.example.quach.food.models.Recipes("Mango Sorbet", "by John Doe", "@drawable/img3","");
        com.example.quach.food.models.Recipes r4 = new com.example.quach.food.models.Recipes("Strawberry Sorbet", "by Steve Thomas", "@drawable/img4","");
        com.example.quach.food.models.Recipes r5 = new com.example.quach.food.models.Recipes("Apple Sorbet", "by Mark Kratzman", "@drawable/img5","");
        com.example.quach.food.models.Recipes r6 = new com.example.quach.food.models.Recipes("Sweet Sorbet", "by Selene Setty", "@drawable/img6","");
        arrData.add(r1);
        arrData.add(r2);
        arrData.add(r3);
        arrData.add(r4);
        arrData.add(r5);
        arrData.add(r6);
    }
}
