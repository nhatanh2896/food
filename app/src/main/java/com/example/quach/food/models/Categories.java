package com.example.quach.food.models;

/**
 * Created by quach on 13/07/17.
 */

public class Categories {
    private String linkImg;
    private String title;
    private int number;

    public Categories(String linkImg, String title, int number) {
        this.linkImg = linkImg;
        this.title = title;
        this.number = number;
    }

    public String getLinkImg() {
        return linkImg;
    }

    public void setLinkImg(String linkImg) {
        this.linkImg = linkImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
