package com.example.quach.food.models;

/**
 * Created by quach on 12/07/17.
 */

public class Recipes {
    private String title;
    private String author;
    private String linkRecipes;
    private String linkUser;

    public Recipes(String title, String author, String linkRecipes, String linkUser) {
        this.title = title;
        this.author = author;
        this.linkRecipes = linkRecipes;
        this.linkUser = linkUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLinkUser() {
        return linkUser;
    }

    public void setLinkUser(String linkUser) {
        this.linkUser = linkUser;
    }

    public String getLinkRecipes() {
        return linkRecipes;
    }

    public void setLinkRecipes(String linkRecipes) {
        this.linkRecipes = linkRecipes;
    }
}
